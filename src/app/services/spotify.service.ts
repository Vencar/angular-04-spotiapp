import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { 
    console.log("Spoty Service Works");
  }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBXtj4osL2pJwlGQwDV9dC43ZJk8adaknjwAMoS4whDXAITaZ2vIXwVR_j4LMtGpiJInWPFzoBXmIuy4Qs'
    });

    return this.http.get(url, {headers});

  }

  getNewReleases() {
    return this.getQuery('browse/new-releases')
    .pipe( map( data=> data['albums'].items));
  }

  getArtistas(termino: string) {
    return this.getQuery(`search?q=${termino}&type=artist&limit=15`)
    .pipe( map(data=> data['artists'].items));
  }

  getArtista(id: string) {
    return this.getQuery(`artists/${ id }`);
  }

  getTopTracks(id: string) {
    return this.getQuery(`artists/${id}/toptracks?country=us`)
    .pipe( map( data => data['tracks'] ));
  }

}
